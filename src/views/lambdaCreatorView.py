import wx
from controllers.awsQueries import AwsQueries 
from controllers.creacionLambda import CreateLambda
from datetime import datetime

class LambdaCreatorView(wx.Frame):

    def __init__(self, parent):
        wx.Frame.__init__(self, None, title=("Creador de lambdas"), size=(1000,800))
        self.SetBackgroundColour(wx.WHITE)

        #variables app
        self.parent = parent
        self.manejadorLambdaCreator = CreateLambda(profile=parent.usuario.perfil_aws,
                                                    usr_git=parent.usuario.usr_git,
                                                    pass_git=parent.usuario.pass_git)

        self.dicVpc = self.manejadorLambdaCreator.getVpc()
        self.dicSegurityGroup = self.manejadorLambdaCreator.getSegurityGroups()
        self.dicRoles = self.manejadorLambdaCreator.listRoles()

        self.listVpc = list(self.dicVpc.keys())
        self.SegurityGroupsList = [sg['GroupName'] for sg in self.dicSegurityGroup if sg['VpcId'] == self.listVpc[0] ]
        self.rolesList = [rol['RoleName'] for rol in self.dicRoles]

        self.listaRuntimes = open('assets/runtimes', 'r').read().split(',')
        
        #sizers
        self.mainSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.sizerUno = wx.StaticBoxSizer(wx.VERTICAL, self, "Datos del Repositorio")
        self.sizerDos = wx.StaticBoxSizer(wx.VERTICAL, self, "Datos de la Funcion Lambda")
        self.sizerTres = wx.BoxSizer(wx.VERTICAL)
        self.sizerCuatro = wx.GridSizer(3, 2, 1, 1)
        self.sizerCinco = wx.GridSizer( 2, 1, 1)
        self.sizerSeis = wx.BoxSizer(wx.HORIZONTAL)
        self.sizerSiete = wx.BoxSizer(wx.HORIZONTAL)
        #imagenes

        #botones
        self.btnCrear = wx.Button(self, label = "Crear Lambda")
        self.btnCrear.Bind(wx.EVT_BUTTON, self.crearLambdaEvt)

        self.btnVolver = wx.Button(self, label = "Volver")
        self.btnVolver.Bind(wx.EVT_BUTTON, self.cancelarEvt)

        #checkboxes
        self.checkPrivate = wx.CheckBox(self, label = "Private")
        self.txtPublish = wx.CheckBox(self, label="Publicar")
        #textos
        #datos del repositorio
        self.txtRepositorio = wx.StaticText(self, label="Repositorio")
        self.txtRama = wx.StaticText(self, label="Rama")
        self.txtUbicacionZip = wx.StaticText(self, label="Ubicacion del .zip")
        #datos de la lambda
        self.txtNombre = wx.StaticText(self, label="Nombre")
        self.txtDescripcion = wx.StaticText(self, label="Descripcion")
        self.txtHandler = wx.StaticText(self, label="Handler")
        self.txtRole = wx.StaticText(self, label="Rol")
        self.txtRuntime = wx.StaticText(self, label="Runtime")
        self.txtSubRedes = wx.StaticText(self, label="Sub Redes")
        self.txtGruposSeguridad = wx.StaticText(self, label="Grupos de Seguridad")
        self.txtVpcs = wx.StaticText(self, label="Vpc")

        #consola
        self.txtLogs = wx.StaticText(self, label="Logs")

        #listCheckBoxes
        self.listCheckSubRedes = wx.CheckListBox(self, choices=self.dicVpc[self.listVpc[0]] )
        self.listCheckGruposSeguridad = wx.CheckListBox(self, choices=self.SegurityGroupsList )

        #inputs
        self.inputRepositorio = wx.TextCtrl(self)
        self.inputRama = wx.TextCtrl(self)
        self.inputUbicacionZip = wx.TextCtrl(self)
        self.inputNombre = wx.TextCtrl(self)
        self.inputDescripcion = wx.TextCtrl(self)
        self.inputHandler = wx.TextCtrl(self)
        self.inputRuntime = wx.ComboBox(self, choices=self.listaRuntimes)
        self.inputRuntime.SetSelection(0)
        self.inputLogs = wx.TextCtrl(self, style=  wx.TE_MULTILINE|wx.BORDER_SUNKEN|wx.TE_READONLY|wx.TE_RICH2)

        #combo boxes
        self.comboVpc = wx.ComboBox(self, choices= self.listVpc)
        self.comboVpc.SetSelection(0)
        self.comboVpc.Bind(wx.EVT_COMBOBOX, self.listSubnets)
        self.inputRole = wx.ComboBox(self, choices= self.rolesList)
        self.inputRole.SetSelection(0)
        self.inputRole.SetMaxSize(wx.Size(250, -1))

        #organizar sizer
        #datos repositorio
        self.sizerCuatro.Add(self.txtRepositorio, 1,  wx.CENTER | wx.ALL, 2)
        self.sizerCuatro.Add(self.inputRepositorio, 1, wx.EXPAND | wx.CENTER | wx.ALL, 2)
        self.sizerCuatro.Add(self.txtRama, 1,  wx.CENTER | wx.ALL, 2)
        self.sizerCuatro.Add(self.inputRama, 1, wx.EXPAND | wx.CENTER | wx.ALL, 2)
        self.sizerCuatro.Add(self.txtUbicacionZip, 1,  wx.CENTER | wx.ALL, 2)
        self.sizerCuatro.Add(self.inputUbicacionZip, 1,  wx.EXPAND | wx.CENTER | wx.ALL, 2)        

        self.sizerUno.Add(self.sizerCuatro, 1.5, wx.EXPAND | wx.ALL, 2)
        self.sizerUno.Add(self.checkPrivate, 0.5, wx.EXPAND, 2)

        #datos lambda
        self.sizerCinco.Add(self.txtNombre ,1,  wx.CENTER | wx.ALL, 2)
        self.sizerCinco.Add(self.inputNombre ,1,  wx.EXPAND | wx.CENTER | wx.ALL, 2)
        self.sizerCinco.Add(self.txtDescripcion ,1,  wx.CENTER | wx.ALL, 2)
        self.sizerCinco.Add(self.inputDescripcion ,1, wx.EXPAND | wx.CENTER | wx.ALL, 2)
        self.sizerCinco.Add(self.txtHandler ,1,  wx.CENTER | wx.ALL, 2)
        self.sizerCinco.Add(self.inputHandler ,1, wx.EXPAND | wx.CENTER | wx.ALL, 2)
        self.sizerCinco.Add(self.txtRole ,1,  wx.CENTER | wx.ALL, 2)
        self.sizerCinco.Add(self.inputRole ,1, wx.EXPAND | wx.CENTER | wx.ALL, 2)
        self.sizerCinco.Add(self.txtRuntime ,1,  wx.CENTER | wx.ALL, 2)
        self.sizerCinco.Add(self.inputRuntime ,1, wx.EXPAND | wx.CENTER | wx.ALL, 2)
        self.sizerCinco.Add(self.txtPublish, 0, wx.ALL, 2)
        self.sizerCinco.AddSpacer(1)
        self.sizerCinco.Add(self.txtVpcs, 1, wx.CENTER | wx.ALL, 2)
        self.sizerCinco.Add(self.comboVpc, 1, wx.CENTER | wx.ALL, 2)
        self.sizerCinco.Add(self.txtSubRedes, 1, wx.ALL, 2)
        self.sizerCinco.Add(self.txtGruposSeguridad, 1, wx.ALL, 2)

        self.sizerSiete.Add(self.listCheckSubRedes,  1, wx.EXPAND | wx.ALL, 2)
        self.sizerSiete.Add(self.listCheckGruposSeguridad,  1, wx.EXPAND | wx.ALL, 2)

        self.sizerDos.Add(self.sizerCinco, 1, wx.CENTER | wx.EXPAND | wx.RIGHT | wx.LEFT, 35)
        self.sizerDos.Add(self.sizerSiete, 2, wx.EXPAND | wx.RIGHT | wx.LEFT , 35)

        self.sizerSeis.Add(self.btnCrear, 1, wx.CENTER | wx.ALL, 2)
        self.sizerSeis.Add(self.btnVolver, 1, wx.CENTER | wx.ALL, 2)

        self.sizerTres.Add(self.sizerUno, 1,  wx.ALL | wx.EXPAND, 1)
        self.sizerTres.Add(self.txtLogs, 0,  wx.TOP, 20)
        self.sizerTres.Add(self.inputLogs, 3,  wx.ALL | wx.EXPAND, 1)
        self.sizerTres.Add(self.sizerSeis, 1,  wx.ALIGN_CENTER_HORIZONTAL | wx.TOP , 20)

        self.mainSizer.Add(self.sizerDos, 1, wx.EXPAND | wx.ALL, 15)
        self.mainSizer.Add(self.sizerTres, 1, wx.EXPAND | wx.ALL, 15)
        

        #acabados
        self.SetSizer(self.mainSizer)
        self.Layout()
        self.Centre()

        #eventos
    
    def crearLambdaEvt(self, evt):

        datos = {
            #repositorio
            'privado':self.checkPrivate.GetValue(),
            'url': self.inputRepositorio.GetValue(),
            'rama': 'main' if not self.inputRama.GetValue() else self.inputRama.GetValue(), 
            'zip_name':self.inputUbicacionZip.GetValue(),
            #lambda
            'perfil':self.parent.usuario.perfil_aws,
            'descripcion':self.inputDescripcion.GetValue(),
            'nombre':self.inputNombre.GetValue(),
            'handler':True if not self.inputHandler.GetValue() else self.inputHandler.GetValue(),
            'publish':self.txtPublish.GetValue(),
            'rol':self.dicRoles[self.inputRole.GetSelection()]['Arn'],
            'runtime':self.inputRuntime.GetValue(),
            'vpc_config':{
                'SubnetIds': list(self.listCheckSubRedes.GetCheckedStrings()),
                'SecurityGroupIds': [ idG['GroupId'] for i, idG in enumerate(self.dicSegurityGroup) if i in self.listCheckGruposSeguridad.GetCheckedItems()]
            }
        }
        datos_validados = self.validateData(datos)
        if datos_validados:
            wx.MessageBox('Los siguientesd campos son requeridos:\n'+str(datos_validados), 'Informacion', style=wx.OK | wx.ICON_WARNING)

        else:

            try :
                self.appendTextConsole("Clonando Repositorio ...")
                self.manejadorLambdaCreator.cloneRepocitorio(**datos)
                self.appendTextConsole("Creando Funcion Lambda ...")
                datos_lambda = self.manejadorLambdaCreator.lambda_creator(**datos)
                self.appendTextConsole("Lambda Creada ...")
                self.appendTextConsole("Informacion : \n%s"%str(datos_lambda))
            except Exception as error:
                self.appendTextConsole("Error : \n%s"%str(error))


    def cancelarEvt(self, evt):
        self.parent.Show()
        self.Destroy()
    
    def listSubnets(self, evt):
        newVpcSelection = self.comboVpc.GetStringSelection()
        self.listCheckSubRedes.Set(self.dicVpc[newVpcSelection] )
        self.SegurityGroupsList = [sg['GroupName'] for sg in self.dicSegurityGroup if sg['VpcId'] == newVpcSelection ]
        self.listCheckGruposSeguridad.Set(self.SegurityGroupsList)
        print("vpc seleccionada : ", newVpcSelection)
        # self.Fit()
    
    def appendTextConsole(self, msj):
        self.inputLogs.AppendText('[info] %s : %s \n'%(datetime.now(), msj))

    def validateData(self, data):
        campos = [key for key in data.keys() if  data[key] == '' or data[key] == None]          
        return campos