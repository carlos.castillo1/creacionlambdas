import wx
from controllers.awsQueries import AwsQueries
import os
from models.usuario import Usuario

class ConfiguracionCredencialesView(wx.Frame):

    awsQuery = AwsQueries(profile=None)

    def __init__(self, parent):
        wx.Frame.__init__(self, None, title=("Configuracion de Credenciales"), size=(650,330))
        self.SetBackgroundColour(wx.WHITE)
        self.parent = parent
        self.perfiles_aws = self.awsQuery.getProfiles()

        #sizer
        self.sizerUno = wx.BoxSizer(wx.VERTICAL)
        self.sizerDos = wx.GridSizer(3, 2, 1, 2)
        self.sizerTres = wx.BoxSizer(wx.HORIZONTAL)

        #imagenes de botones
        picBtnVolver = wx.Bitmap("assets/hacia-atras.png", wx.BITMAP_TYPE_ANY)
        picGuardar = wx.Bitmap("assets/guardar.png", wx.BITMAP_TYPE_ANY)

        #botones con imagenes
        
        self.btnVolver = wx.BitmapButton(self, bitmap=picBtnVolver, style=wx.ALIGN_LEFT)
        # self.btnVolver.SetMaxSize(wx.Size(50, 50))
        self.btnVolver.Bind(wx.EVT_BUTTON, self.volverAction)
        
        self.btnGuardar = wx.BitmapButton(self, bitmap=picGuardar, style=wx.ALIGN_RIGHT)
        # self.btnGuardar.SetMaxSize(wx.Size(50, 50))
        self.btnGuardar.Bind(wx.EVT_BUTTON, self.guardarConfig)

        #static text
        self.txtAwsProfile = wx.StaticText(self, -1, "Perfil - AWS")
        self.txt_usr_git = wx.StaticText(self, -1, "usuario (GIT)")
        self.txt_pass_git = wx.StaticText(self, -1, "passwor (git)")

        #text control
        self.input_usr_git = wx.TextCtrl(self)
        self.input_pass_git = wx.TextCtrl(self)

        #combo boxes
        self.comboPerfiles = wx.ComboBox(self, choices=self.perfiles_aws)

        #dialogs

        #oraganizando sizers

        self.sizerTres.Add(self.btnVolver, 1, wx.ALL , 5)
        self.sizerTres.Add(self.btnGuardar, 1, wx.ALL , 5)

        self.sizerDos.Add(self.txtAwsProfile, 1,  wx.CENTER, 2)
        self.sizerDos.Add(self.comboPerfiles, 1, wx.ALL | wx.CENTER | wx.EXPAND, 2)
        self.sizerDos.Add(self.txt_usr_git, 1,wx.ALL | wx.CENTER, 2)
        self.sizerDos.Add(self.input_usr_git, 1, wx.ALL | wx.CENTER | wx.EXPAND, 2)
        self.sizerDos.Add(self.txt_pass_git, 1, wx.ALL | wx.CENTER, 2)
        self.sizerDos.Add(self.input_pass_git, 1, wx.ALL | wx.CENTER | wx.EXPAND, 2)

        self.sizerUno.Add(self.sizerDos, 5, wx.ALL | wx.EXPAND , 30)
        self.sizerUno.Add(self.sizerTres, 1, wx.ALL| wx.EXPAND , 5)

        #acabados
        self.SetSizer(self.sizerUno)
        self.Layout()
        self.Centre()
    
    def volverAction(self, evt):
        self.parent.Show()
        self.Destroy()
    
    def guardarConfig(self, evt):
        datos = {
            'perfil_aws': self.comboPerfiles.GetStringSelection(),
            'usr_git' : os.getenv(self.input_usr_git.GetValue()),
            'pass_git' : os.getenv(self.input_pass_git.GetValue())
        }
        validate_data = self.validateData(datos)
        if validate_data :
            wx.MessageBox('Las siguientes variables de entorno no existen: \n'+str(validate_data), 'Informacion', style=wx.OK | wx.ICON_WARNING)
        else:
            self.parent.usuario= Usuario(**datos)
            wx.MessageBox('informacion','configuracion guardada ', style=wx.OK | wx.ICON_INFORMATION)
            self.Destroy()
            self.parent.Show()
    
    def validateData(self, data):
        campos = [key for key in data.keys() if  data[key] == '' or data[key] == None]
        return campos