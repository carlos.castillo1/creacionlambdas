import wx
from views.configuracionCredencialesView import ConfiguracionCredencialesView
from views.lambdaCreatorView import LambdaCreatorView
from models.usuario import Usuario
from git import Repo, Git
from utils import validaciones
import ctypes

class MainFrame(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, None, title=("Lambda Creator"), size=(650,400))
        self.SetBackgroundColour(wx.WHITE)

        #varaibles
        self.usuario = Usuario()

        #sizer
        self.sizerUno = wx.BoxSizer(wx.HORIZONTAL)

        #imagenes de botones
        configPic = wx.Bitmap("assets/ajustes.png", wx.BITMAP_TYPE_ANY)
        lambdaTool = wx.Bitmap("assets/wrench.png", wx.BITMAP_TYPE_ANY)

        #botones y eventos
        self.btnConfiguracion = wx.BitmapButton(self, bitmap=configPic)
        self.btnConfiguracion.Bind(wx.EVT_BUTTON, self.openConfigCredentials)
        self.btnLambdaCreator = wx.BitmapButton(self, bitmap=lambdaTool)
        self.btnLambdaCreator.Bind(wx.EVT_BUTTON, self.openLambdaCreator)

        #configurar sizers
        self.sizerUno.Add(self.btnConfiguracion, 1, wx.ALL | wx.CENTER , 45)
        self.sizerUno.Add(self.btnLambdaCreator, 1, wx.ALL | wx.CENTER , 45)

        #acabados
        self.SetSizer(self.sizerUno)
        self.Layout()
        self.Centre()
    
    def openConfigCredentials(self, evt):
        self.Hide()
        frameConfigCredentials = ConfiguracionCredencialesView(parent=self)
        frameConfigCredentials.Show()
    
    def openLambdaCreator(self, evt):
        validate_data = validaciones.validateData(self.usuario.__dict__)
        if validate_data:
            wx.MessageBox('Los siguientes campos son requeridos:\n'+str(validate_data), 'Informacion', style=wx.OK | wx.ICON_WARNING)
        else :
            lambdaCreatorFrame = LambdaCreatorView(parent=self)
            lambdaCreatorFrame.Show()
            self.Hide()

if __name__ == "__main__":
    app = wx.App()        
    v = MainFrame()
    v.Show()
    app.MainLoop()


