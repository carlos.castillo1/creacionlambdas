from .gitControlador import GitControlador
from utils.settings import *
from .awsQueries import AwsQueries
import shutil


class CreateLambda(GitControlador, AwsQueries):

    def __init__(self, **kwargs):
        super().__init__(usr_git=kwargs['usr_git'], pass_git=kwargs['pass_git'])
        super(GitControlador, self).__init__(profile=kwargs['profile'])

    def get_zip_file(self, zipname):
        urlZip = TMP + '/' + zipname
        with open(urlZip, 'rb') as file_data:
            bytes_content = file_data.read()
        shutil.rmtree(TMP)
        self.createTmp()
        return bytes_content


    def lambda_creator(self, **kwargs):
        lambda_client = self.botoMotor.client('lambda')
        response = lambda_client.create_function(
            Code={
                'ZipFile': self.get_zip_file(kwargs['zip_name'])
            },
            Description=kwargs['descripcion'],
            FunctionName=kwargs['nombre'],
            Handler= '' if not kwargs['handler'] else kwargs['handler'],
            Publish=kwargs['publish'],
            Role=kwargs['rol'],
            Runtime=kwargs['runtime'],
            VpcConfig=kwargs['vpc_config'],
        )
        return response
    
    def run_creator(self, **kwargs):
        self.cloneRepocitorio(**kwargs)
        return self.lambda_creator(**kwargs)