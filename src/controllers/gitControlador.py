import os
from os import path
from git import Repo
from utils.settings import *


class GitControlador():

    def __init__(self, **kwargs):
        self.directorio = DIRECTORIO
        self.tmp = TMP
        self.username = kwargs['usr_git']
        self.password = kwargs['pass_git']
        self.createTmp()

    def createTmp(self):
        if not path.exists(path.exists(self.tmp)):
            try:
                os.mkdir(self.tmp)
            except OSError:
                print("No se pudo crear el directorio tmp en la ruta %s \n fallo" % path)
            else:
                print("tmp creado %s " % path)

    def cloneRepocitorio(self, **kwargs):
        if kwargs['privado']:
            kwargs['url'] = self.getUrlByPrivateGit(kwargs['url'])
        Repo.clone_from(kwargs['url'], self.tmp, branch=kwargs['rama'])

    #esta funcioan agrega las credenciales del usuario git a la url del repositorio
    def getUrlByPrivateGit(self, url):
        repositorio = url.split("//")
        repositorio[0] = repositorio[0] + '//'
        repositorio.insert(1, "%s:%s@" % (self.username, self.password))
        return ''.join(repositorio)
