import boto3

class AwsQueries():
    
    def __init__(self, **kwargs):
        print("si hay profile : ", kwargs)
        if kwargs['profile']:
            self.createSession(kwargs['profile'])
            print("si hay profile : ", kwargs['profile'])

    def getProfiles(self):
        return [profile for profile in boto3.session.Session().available_profiles]

    def createSession(self, profile):
        self.botoMotor = boto3.session.Session(profile_name=profile) 

    def getVpc(self):
        vpcSerializer = {}
        ec2 = self.botoMotor.resource("ec2")
        for vpc in ec2.vpcs.all():
            vpcSerializer[vpc.id] = []
            for subnet in vpc.subnets.all():
                vpcSerializer[vpc.id].append(subnet.id)
        return vpcSerializer
    
    def getSegurityGroups(self):
        return self.botoMotor.client('ec2').describe_security_groups()['SecurityGroups']
        # return [ sg['VpcId'] for sg in self.botoMotor.client('ec2').describe_security_groups()['SecurityGroups']]
    
    def listRoles(self):
        return self.botoMotor.client('iam').list_roles()['Roles']