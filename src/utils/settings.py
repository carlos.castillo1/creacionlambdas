import os
from sys import platform
#VARIABLES DE LA APP

DIRECTORIO = os.getcwd()
TMP = DIRECTORIO + '/tmp'



#CONFIGURACION REQUERIDA

#credenciales aws cli
AWS_ACCESS_KEY_ID =  'AWS_ACCESS_KEY_ID'
AWS_SECRET_ACCESS_KEY = 'AWS_SECRET_ACCESS_KEY'
AWS_REGION = 'AWS_REGION'

#credenciales git
USR_GIT = 'USR_GIT'
PASS_GIT = 'PASS_GIT'


#varaibles de configuracion para la crecion de una lambda
#configuracin del repositorio donde esta el zip
REPOSITORIO = 'https://gitlab.com/carlos.castillo1/creacionlambdas.git'
REPO_IS_PRIVATE = False
RAMA = 'main'
ZIPNAME = 'my-deployment-package.zip'

#configuracion de la lambda
DESCRIPCION = 'descripcion lambda test'
NOMBRE = 'lambdaPythonTest'
HANDLER = 'creacionlambdas.lambdaFunction.main'
PUBLISH = ''
ROLE = 'arn:aws:iam::248123570293:role/service-role/LambdaTipoIdentificacion-role-1hl5n3yg'
RUNTIME = 'python3.9'
SUB_REDES_IDS = ['id_subnet','id_subnet_dos']
SEGURITY_GROUPS_IDS = ['SEG_GROUP', 'SEG_GROUP_DOS']
