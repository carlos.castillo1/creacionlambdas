# Aplicacion de escritorio para crear funciones lambda desde un repositorio

La aplicacion crea funciones lambda en amazon web services cuyo código fuente se encuentra en un repositorio de forma comprimida en formato zip 

## Instalación

* debe tener una versión de python mayor a la 3.6. 
* Parece en la raíz del proyecto y ejecute el siguiente código:
    `python -m pip install -r requirements.`


## Funcionamiento 
A continuacion se describen los archivos principales de la aplicacion

* **utils.settings.py**
    
    Este archivo guarda las variables de los directorios que utiliza la app

* **controllers.gitControlador.GitControlador**

    Este archivo utiliza las variables que estan en el archivo setting.py. Utiliza credenciales de git las cuales se pasan en el metodo constructor de la clase, con esto la clase puede descargar un repositorio y guardarlo en una carpeta de archivos temporales llamada 'tmp', esta funcion la hace el metodo `cloneRepocitorio(**kwargs)` que recibe los parametros del repositorio: url, rama, etc

* **controllers.creacionLambda.CreateLambda**

    Esta clase hereda de `GitControlador` para manejar el contenido del repositorio donde esta alojado el codigo fuente de la lambda en formato zip.Por si misma crea la funcion lambda con el metodo `lambda_creator(**kwargs)`,

    `run_creator(**kwargs)` Esta función hace dos cosas:

    **1.** llama el método `cloneRepocitorio(**kwargs)` para clonar el repositorio donde se encuentra el código fuente de la lambda.
    
    **2.** llama el método `lambda_creator(**kwargs)` que crea la función lambda con el codigo fuente que se descargó en el paso anterior y la configuración de la lambda que se paso por parametro

***La app tiene tres interfaz grafica desarrollada con la biblioteca wxPyton***

* **main.py** 

    Es la interfaz principal, solo muestra dos botones al usuario: uno para ir a la configuracion y
    el otro para ir a la herramienta para crear lambdas.

* **views.ConfiguracionCredencialesView**
    
    Esta interfaz tiene el formulario para escoger los perfiles de aws-cli que estab su computadora y el nombre de las variables de entorno que guardan las credenciales del usuario git.
    Si no se completa este formulario no se puede ingresar a la siguiente interfaz de usuario.

* **views.LambdaCreatorView**

    Esta interfaz tiene el formulario de la configuracion de la lambda y el repositorio de donde se descargara el codigo fuente de la funcion, tiene un cuadro de texto que muestra los procesos que tuvo que hacer para crear la lambda.
    Esta clase utiliza la clase CreateLambda para crear la funcion lambda.


**Crear archivo ejecutable .exe**

Debe instalar la libreria pyInstaller
En windows ejecute `pip install pyinstaller`

luego en la raiz del proyecto ejecute `pyinstaller --onefile src/main.py`

esto creara una carpeta llamada 'dist' y su interior se encuentra el archivo ejecutable. EL archivo debe colocarse al lado de la carpeta 'assets' para que pueda acceder a los recursos estaticos que utiliza la aplicacion.

## ¿Cómo ejecutar?
el comando creara dos carpetas 'build' y 'dist', detro de 'dist' esta el ejecutable, pongalo al lado de la carpeta assets para que pueda acceder a los recursos estaticos y ejecutelo.

## Para probar
Puede intentar subir la funcion lambda que se encuentra dentro del archivo 'my-deployment-package.zip' en este mismo repositorio, esta devuelve un texto que dice 'hello world!'. Podria utilizar este repositorio y este archivo para probar la aplicacion.